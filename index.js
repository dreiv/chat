const express = require('express')

const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)

const sockets = []

app.get('/', (req, res) => {
	res.send({ hello: 'world' })
})

const PORT = process.env.PORT || 5000
io.on('connection', socket => {
	console.log('a user connected')
	socket.on('disconnect', () => {
		sockets.splice(sockets.indexOf(socket), 1)
	})
	socket.on('circleMoved', coords => {
		sockets.forEach(client => {
			client.emit('circleMoved', coords)
		})
	})
	sockets.push(socket)
})

http.listen(PORT, () => {
	console.log('listening on *:3000')
})
